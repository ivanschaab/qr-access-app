//
//  Guest.swift
//  QR Access
//
//  Created by Ivan Schaab on 9/19/19.
//  Copyright © 2019 Ivan Schaab. All rights reserved.
//

import Foundation

struct Guest {
    let fullName : String
    let familyName : String
    let nationalId : String
    let birthday : String
    
    init(fullName: String, familyName:String, nationalId:String, birthday:String) {
        self.fullName = fullName
        self.familyName = familyName
        self.nationalId = nationalId
        self.birthday = birthday
    }
    
    func getAge(birthday : String) -> Int {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        let now = Date()
        let birthdays: Date = dateFormatter.date(from:birthday)!
        let calendar = Calendar.current
        
        let ageComponents = calendar.dateComponents([.year], from: birthdays, to: now)
        return ageComponents.year!
    }
}
extension Guest {
    var age : Int {
        return getAge(birthday: self.birthday)
    }
    
    var accessDate: String {
        let currentDate = Date()
        let format = DateFormatter()
        
        format.timeZone = .current
        format.dateFormat = "dd/MM/yyyy HH:mm"
        
        return format.string(from: currentDate)
    }
}

