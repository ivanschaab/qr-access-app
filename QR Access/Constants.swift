//
//  Constants.swift
//  QR Access
//
//  Created by Ivan Schaab on 9/20/19.
//  Copyright © 2019 Ivan Schaab. All rights reserved.
//
import UIKit
import AVFoundation

struct Constants {
    
    //MARK: MODEL URL
    static let CSV_GUEST_LIST_URL = "http://lobomkt.com/csv.csv"
    static let CSV_EXPORT_COLUMNS = "DNI,Apellido,Nombre,Edad,Cumpleaños,Hora De Ingreso\n"
 
    static let PDF_417_TYPE =  [AVMetadataObject.ObjectType.pdf417]
    //MARK: UI
    static let VIEW_CORNER_RADIUS: CGFloat = 10

}
