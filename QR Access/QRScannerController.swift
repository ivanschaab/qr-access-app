//
//  QRScannerController.swift
//  QR Access
//
//  Created by Ivan Schaab on 11/09/2019.
//  Copyright © 2019 Ivan Schaab. All rights reserved.
//


import UIKit
import AVFoundation
import SwiftCSV

class QRScannerController: UIViewController {
    
    @IBOutlet var topbar: UIView!
    @IBOutlet weak var lightButton: UIButton!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var fullName: UILabel!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var familyName: UILabel!
    @IBOutlet weak var nationalId: UILabel!
    @IBOutlet weak var birthday: UILabel!
    @IBOutlet weak var accessViewStatus: UIView!
    @IBOutlet weak var exportButton: UIButton!
    
    var guestList : [Guest] = []
    var passedList : [Guest] = []
    
    var guestScaned = Guest(fullName: "Default", familyName: "Default", nationalId: "Default", birthday: "1/1/1968")
    var captureSession = AVCaptureSession()
    var qrString: String = ""
    
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var qrCodeFrameView: UIView?
    
    //MARK: CONTROLLER METHODS
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        accessViewStatus.layer.cornerRadius = accessViewStatus.frame.height / 2
        cameraView.layer.cornerRadius = Constants.VIEW_CORNER_RADIUS
        
        downloadGuestListFromServer()
        
        initLables()
        
        
        configureCamera()
        
        // Move the message label and top bar to the front
        view.bringSubviewToFront(topbar)
        view.bringSubviewToFront(lightButton)
        
        // Initialize QR Code Frame to highlight the QR code
        qrCodeFrameView = UIView()
        
        if let qrCodeFrameView = qrCodeFrameView {
            qrCodeFrameView.layer.borderColor = UIColor.green.cgColor
            qrCodeFrameView.layer.borderWidth = 2
            view.addSubview(qrCodeFrameView)
            view.bringSubviewToFront(qrCodeFrameView)
        }
    }
    
    fileprivate func initLables() {
        fullName.text = guestScaned.fullName
        age.text = String(guestScaned.age)
        familyName.text = guestScaned.familyName
        nationalId.text = guestScaned.nationalId
        birthday.text = guestScaned.birthday
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.captureSession.startRunning()
    }
    
    //MARK: DOWNLOAD CSV

    fileprivate func downloadGuestListFromServer() {
        let url = URL(string: Constants.CSV_GUEST_LIST_URL)!
        
        let task = URLSession.shared.downloadTask(with: url) { localURL, urlResponse, error in
            if let localURL = localURL {
                
                if let string = try? String(contentsOf: localURL) {
                    
                    let csv = CSV(string: string,delimiter: "\r")
                    
                    print(csv.description)
                    
                    for row in csv.enumeratedRows {
                        let guestdata = row[0].components(separatedBy: ";")
                        
                        self.guestList.append(Guest(fullName: guestdata[1], familyName:"", nationalId: guestdata[0], birthday:""))
                    }
                    
                    print(self.guestList)
                }
            }
        }
        
        task.resume()
    }
    
    fileprivate func configureCamera(){
        // Get the back-facing camera for capturing videos
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera,.builtInWideAngleCamera,.builtInTelephotoCamera], mediaType: AVMediaType.video, position: .back)
        
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
            // Initialize a AVCaptureMetadataOutput object and set it as the output device to the capture session.
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            
            // Set delegate and use the default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = Constants.PDF_417_TYPE
            //            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        // Initialize the video preview layer and add it as a sublayer to the viewPreview view's layer.
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = cameraView.layer.bounds
        videoPreviewLayer?.borderWidth = 4
        videoPreviewLayer?.cornerRadius = Constants.VIEW_CORNER_RADIUS
        videoPreviewLayer?.borderColor = UIColor.gray.cgColor
        cameraView.layer.addSublayer(videoPreviewLayer!)
        
        // Start video capture.
        captureSession.startRunning()
        
    }
    
    //MARK: BUTTONS ACTIONS
    @IBAction func exportAction(_ sender: Any) {
        if self.passedList.count > 0 {
            self.createExplortPassedGuestCSV()
        }else{
            let alert = UIAlertController(title: "Alert", message: "Nada para Exportar", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK",
                                          style: .default,
                                          handler: {(alert: UIAlertAction!) in print("NOTHING TO EXPORT")}))
            
            self.present(alert, animated: true)
        }
    }
    
    @IBAction func ligthButtonAction(_ sender: Any) {
        guard let device = AVCaptureDevice.default(for: AVMediaType.video) else { return }
        guard device.hasTorch else { return }
        
        do {
            try device.lockForConfiguration()
            
            if (device.torchMode == AVCaptureDevice.TorchMode.on) {
                device.torchMode = AVCaptureDevice.TorchMode.off
            } else {
                do {
                    try device.setTorchModeOn(level: 1.0)
                } catch {
                    print(error)
                }
            }
            
            device.unlockForConfiguration()
        } catch {
            print(error)
        }
    }
    //MARK: CHECK USER SCANED & QR

    fileprivate func checkLists() -> Bool {
        for user in self.guestList{
            
            
           
            if guestScaned.nationalId == user.nationalId{
                for userPassed in self.passedList{
                    if userPassed.nationalId == user.nationalId{
                        
                        let alert = UIAlertController(title: "Alert", message: "YA PASO TRAMPOSO", preferredStyle: .alert)

                        alert.addAction(UIAlertAction(title: "OK",
                                                          style: .default,
                                                          handler: {(alert: UIAlertAction!) in self.captureSession.startRunning()}))

                        self.present(alert, animated: true)
                        
                        return false//YA PASO!
                    }
                }
                self.passedList.append(guestScaned)
                return true
            }
        }
        return false
    }
    
    fileprivate func checkQR(){
        var access = false
        let dniInfoParced = qrString.components(separatedBy: "@")
        
        guestScaned = Guest(fullName: dniInfoParced[2], familyName: dniInfoParced[1], nationalId: dniInfoParced[4], birthday: dniInfoParced[6])
        
        fullName.text = guestScaned.fullName.capitalized
        age.text = String(guestScaned.age)
        familyName.text = guestScaned.familyName.capitalized
        nationalId.text = guestScaned.nationalId
        birthday.text = guestScaned.birthday
        
        if guestScaned.age < 18 {
            age.textColor = UIColor.red
            age.text?.append(contentsOf: " <- Menor De Edad")
            access = false
        }else {
            access = checkLists()
            age.textColor = UIColor.black
        }
        
        
        if access {
            accessViewStatus.backgroundColor = UIColor.green
        }else{
            accessViewStatus.backgroundColor = UIColor.red
        }
        
        print(self.passedList)
    }
    
    //MARK: EXPORT CSV
    
    fileprivate func createExplortPassedGuestCSV() {
        
        let fileName = "\(UIDevice.current.name)_GuestPassed.csv"
        let path = NSURL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)!
        var csvText = Constants.CSV_EXPORT_COLUMNS
        for user in passedList {
            let newLine = "\(user.nationalId),\(user.familyName),\(user.fullName),\(user.age),\(user.birthday),\(user.accessDate)\n"
            csvText.append(contentsOf: newLine)
        }
        
        do {
            try csvText.write(to: path, atomically: true, encoding: String.Encoding.utf8)
        } catch {
            print("Failed to create file")
            print("\(error)")
        }
        
        let vc = UIActivityViewController(activityItems: [path], applicationActivities: [])
        present(vc, animated: true, completion: nil)
        if let popOver = vc.popoverPresentationController {
            popOver.sourceView = self.exportButton
            //popOver.sourceRect =
            //popOver.barButtonItem
        }
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var shouldAutorotate: Bool{
        return false
    }
}

extension QRScannerController: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
  
        // Check if the metadataObjects array is not nil and it contains at least one object.
        if metadataObjects.count == 0 {
            videoPreviewLayer?.borderColor = UIColor.gray.cgColor
            qrCodeFrameView?.frame = CGRect.zero
            return
        }
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if Constants.PDF_417_TYPE.contains(metadataObj.type) {
            // If the found metadata is equal to the QR code metadata (or barcode) then update the status label's text and set the bounds
            
            videoPreviewLayer?.borderColor = UIColor.green.cgColor
            
            //            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            
            //            qrCodeFrameView?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                self.captureSession.stopRunning()
                qrString = metadataObj.stringValue!
                checkQR()
            }
        }
    }
}
